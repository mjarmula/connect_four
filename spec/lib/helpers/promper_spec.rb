require 'spec_helper'

RSpec.describe Prompter do
  subject(:instance) { Prompter.new }

  describe '#ask_about' do
    let(:message) { 'Lorem ipsum dolor sit amet' }
    let(:user_input) { 'consectetur adipiscing elit' }

    before do
      allow(STDIN).to receive(:gets).and_return(user_input)
      allow(STDOUT).to receive(:puts)
    end

    it 'renders question message' do
      expect(STDOUT).to receive(:puts).with(message)

      instance.ask_about(message)
    end

    it 'returns user input' do
      expect(instance.ask_about(message)).to eq(user_input)
    end
  end
end
