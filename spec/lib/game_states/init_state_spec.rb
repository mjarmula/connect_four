require 'spec_helper'

RSpec.describe GameStates::InitState do
  let(:gameplay_state) { double(GameStates::GameplayState) }

  before do
    allow(Config).to receive(:instance).and_return(config_instance)
  end

  let(:players) { [:human] }
  let(:available_colors) { [:red] }

  let(:config_instance) do
    double(
      Config,
      players: players,
      available_colors: available_colors
    )
  end

  before do
    allow(Player::Factory).to receive(:get)
    expect_any_instance_of(GameStates::GameplayState).to receive(:start)
  end

  describe '.start' do
    subject { described_class.start }

    let(:welcome_message_part1) do
      '--==] Welcome to the Connect Four game [==--'
    end

    let(:welcome_message_part2) do
      'Please introduce yourself'
    end

    it 'renders welcome message' do
      expect(STDOUT).to receive(:puts).with(welcome_message_part1)
      expect(STDOUT).to receive(:puts).with(welcome_message_part2)
      subject
    end

    context 'when there is one player - human' do
      it 'defines playes' do
        allow(STDOUT).to receive(:puts)

        expect(Player::Factory).to receive(:get)
          .with(type: :human, available_colors: available_colors)
        subject
      end
    end
  end
end
