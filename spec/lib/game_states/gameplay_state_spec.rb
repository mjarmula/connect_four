require 'spec_helper'

RSpec.describe GameStates::GameplayState do
  describe '.start' do
    subject { described_class.start(params) }

    let(:params) { { players: players } }
    let(:players) { [player] }
    let(:player) { double(Player) }
    let(:board) { double(Board, cols: 1, rows: 1) }
    let(:view) { double(GameStates::GameplayState::View::ConsoleView) }
    let(:next_state) { double(GameStates::GameOverState) }

    before do
      allow_any_instance_of(described_class).to receive(:board)
        .and_return(board)
      allow_any_instance_of(described_class).to receive(:players)
        .and_return(players)
      allow_any_instance_of(described_class).to receive(:view)
        .and_return(view)
      allow_any_instance_of(described_class).to receive(:next_state)
        .and_return(next_state)
      allow(view).to receive(:render)
      allow(board).to receive(:show_possible_moves)
      allow(player).to receive(:winner?)
      allow(player).to receive(:make_move)
      allow(next_state).to receive(:start)
    end

    it 'shows possible moves' do
      expect(board).to receive(:show_possible_moves)
      subject
    end

    it 'renders the state view' do
      expect(view).to receive(:render).with(board: board, players: players)
      subject
    end

    context 'when the game is over' do
      context 'when there is a winner' do
        before do
          allow(player).to receive(:winner?).and_return(true)
        end

        it 'goes to the last state with player as a winner' do
          expect(next_state).to receive(:start).with(winner: player)
          subject
        end
      end

      context 'where there is no winner' do
        before do
          allow(player).to receive(:winner?).and_return(false)
        end

        it 'goes to the last state with no winner' do
          expect(next_state).to receive(:start).with(winner: nil)
          subject
        end
      end
    end
  end
end
