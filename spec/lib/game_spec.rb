require 'spec_helper'

RSpec.describe Game do
  describe '.start' do
    subject { described_class.start }

    it 'starts the initial state' do
      expect_any_instance_of(GameStates::InitState).to receive(:start)
      subject
    end
  end
end
