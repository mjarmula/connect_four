require 'spec_helper'

RSpec.describe Board::Render do
  describe '.call' do
    subject { described_class.call(original_matrix) }

    let(:original_matrix) { Array.new(6, Array.new(7, field)) }
    let(:field) { double(Board::Field, to_s: ' ') }

    it 'renders original matrix with column numbers [1-7]' do
      expect(subject).to match(/[1-7]+/)
    end
  end
end
