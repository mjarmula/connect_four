require 'spec_helper'

RSpec.describe Board::Grid do
  subject(:instance) { Board::Grid.new(params) }

  let(:params) { { cols: cols, rows: rows } }
  let(:cols) { 7 }
  let(:rows) { 6 }

  describe '#matrix' do
    it 'returns 7x6 size matrix polulated with fields' do
      expect(subject.matrix.flatten.size).to eq(cols * rows)
      expect(subject.matrix[0][0]).to be_a(Board::Field)
    end
  end

  describe '#fetch_field' do
    context 'when there is a field with given coordinates' do
      let(:x) { 1 }
      let(:y) { 1 }

      it 'returns field' do
        expect(instance.fetch_field(x: x, y: y)).to be_a(Board::Field)
        expect(instance.fetch_field(x: x, y: y).x).to eq(x)
        expect(instance.fetch_field(x: x, y: y).y).to eq(y)
      end
    end

    context 'when there is no field with given coordinates' do
      let(:x) { 1 }
      let(:y) { 7 }

      it 'returns nil' do
        expect(instance.fetch_field(x: x, y: y)).to eq(nil)
      end
    end
  end
end
