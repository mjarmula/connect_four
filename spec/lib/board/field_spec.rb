require 'spec_helper'

RSpec.describe Board::Field do
  subject(:instance) { described_class.new(params) }
  let(:params) { { x: x, y: y, token: token } }
  let(:x) { 0 }
  let(:y) { 0 }
  let(:token) { double(Board::Token, to_s: 'o') }
  let(:token_symbol) { 'x' }

  describe 'token?' do
    context 'when field has token' do
      it 'returns true' do
        expect(instance.token?).to be_truthy
      end
    end

    context 'when field doesnt have any token' do
      let(:token) { nil }

      it 'returns false' do
        expect(instance.token?).to be_falsy
      end
    end
  end

  describe 'mark_as_available' do
    it 'changes field\s value to available' do
      instance.mark_as_available

      expect(instance.instance_variable_get(:@value))
        .to eq(Board::Token::Symbol.on_light_white)
    end
  end

  describe '#to_s' do
    context 'when field has token' do
      it 'returns token\s graphical representation' do
        expect(instance.to_s).to eq(token.to_s)
      end
    end

    context 'when field doesnt have any token' do
      let(:token) { nil }
      let(:empty_value) { ' ' }

      it 'returns empty value' do
        expect(instance.to_s).to eq(empty_value)
      end
    end
  end
end
