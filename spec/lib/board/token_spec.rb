require 'spec_helper'

RSpec.describe Board::Token do
  subject(:instance) { described_class.new }
  let(:owner) { double(Player, color: :red) }

  before do
    allow(instance).to receive(:owner).and_return(owner)
  end

  describe '#to_s' do
    it 'returns graphical representation' do
      expect(instance.to_s).to eq(
        Board::Token::Symbol.colorize(background: owner.color)
      )
    end
  end
end
