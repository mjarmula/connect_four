require 'spec_helper'

RSpec.describe Board do
  subject(:instance) { described_class.new(params) }
  let(:params) { { cols: cols, rows: rows } }
  let(:cols) { 7 }
  let(:rows) { 6 }

  describe '#matrix' do
    it 'gets matrix from Grid object' do
      expect_any_instance_of(Board::Grid).to receive(:matrix)
      instance.matrix
    end
  end

  describe '#show_possible_moves' do
    let(:available_fields) { [field] }
    let(:field) { double(Board::Field, x: 0, y: 0) }

    before do
      expect(Detectors::AvailableFieldsDetector).to receive(:find)
        .and_return(available_fields)
    end

    it 'changes field to look like available' do
      expect(field).to receive(:mark_as_available)

      instance.show_possible_moves
    end
  end

  describe '#to_s' do
    it 'returns graphical projection' do
      expect(Board::Render).to receive(:call).with(instance.matrix)

      instance.to_s
    end
  end
end
