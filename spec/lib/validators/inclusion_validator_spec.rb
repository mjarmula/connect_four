require 'spec_helper'

RSpec.describe Validators::InclusionValidator do
  describe '#validate!' do
    subject { described_class.new(params).validate! }

    let(:params) { { input: input, range: range } }
    let(:input) { 1 }
    let(:range) { 0..4 }

    context 'when the input is included in the range' do
      it 'raises no error' do
        expect { subject }.to_not raise_error
      end
    end

    context 'when the input is not included in the range' do
      let(:input) { 5 }

      it 'raises no error' do
        expect { subject }.to raise_error(Validators::Errors::InvalidInputError)
      end
    end
  end
end
