require 'spec_helper'

RSpec.describe Detectors::WinnerDetector do
  describe '.connected_four?' do
    subject { described_class.connected_four?(params) }

    let(:params) { { board: player, player: player } }
    let(:player) { double(Player) }
    let(:board) { double(Board) }
    let(:tokens) { [token] }
    let(:token) { double(Board::Token, field: field) }
    let(:field) { double(Board::Field) }
    let(:horizontal_detector) { Detectors::WinnerDetector::HorizontalDetector }
    let(:vertical_detector) { Detectors::WinnerDetector::VerticalDetector }
    let(:diagonal_left_detector) do
      Detectors::WinnerDetector::DiagonalLeftDetector
    end

    let(:diagonal_right_detector) do
      Detectors::WinnerDetector::DiagonalRightDetector
    end

    before do
      allow(player).to receive(:tokens).and_return(tokens)
    end

    it 'checks if player has four connection horizontally, vertically or diagonally' do
      expect(horizontal_detector).to receive(:check)
      expect(vertical_detector).to receive(:check)
      expect(diagonal_left_detector).to receive(:check)
      expect(diagonal_right_detector).to receive(:check)

      subject
    end
  end
end
