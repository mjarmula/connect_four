require 'spec_helper'

describe Detectors::AvailableFieldsDetector do
  include_examples 'a detector'

  subject { described_class.find(board: board) }

  before do
    expect(board).to receive(:matrix).at_least(:once).and_return(matrix)
  end

  describe '.find' do
    context 'before first move, the board is empty' do
      let(:expected_response) { [field13, field14, field15, field16] }

      it 'returns only the bottom row' do
        expect(subject).to eq(expected_response)
      end
    end

    context 'after first move, one field of the bottom row is taken' do
      let(:expected_response) { [field12, field13, field14, field15] }

      before do
        allow(field16).to receive(:token?).and_return(true)
      end

      it 'returns previously available fields, istead of taken field returns the one above' do
        expect(subject).to eq(expected_response)
      end
    end
  end
end
