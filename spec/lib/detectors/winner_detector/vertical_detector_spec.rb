require 'spec_helper'

RSpec.describe Detectors::WinnerDetector::VerticalDetector do
  include_examples 'a detector'

  describe '.check' do
    subject { described_class.check(params) }

    let(:params) { { board: board, field: initial_field, player: player } }
    let(:player) { double(Player) }
    let(:token) { double(Board::Token, owner: player) }
    let(:initial_field) { field13 }

    context 'when 4 tokens are connected vertically' do
      before do
        allow(field1).to receive(:token).and_return(token)
        allow(field5).to receive(:token).and_return(token)
        allow(field9).to receive(:token).and_return(token)
      end

      it 'returns true' do
        is_expected.to be_truthy
      end
    end

    context 'when only 3 tokens are connected vertically' do
      before do
        allow(field2).to receive(:token).and_return(token)
        allow(field3).to receive(:token).and_return(token)
      end

      it 'returns false' do
        is_expected.to be_falsy
      end
    end
  end
end
