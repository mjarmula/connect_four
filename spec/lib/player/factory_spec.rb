require 'spec_helper'

RSpec.describe Player::Factory do
  describe '.get' do
    subject { described_class.get(params) }
    let(:params) { { type: type, available_colors: available_colors } }
    let(:available_colors) { [:red, :green] }
    let(:color) { :red }

    context 'when build human player' do
      let(:type) { :human }
      let(:name) { 'Maciej Jarmula' }

      it 'build object by interactive player builder' do
        expect(Player::InteractivePlayerBuilder).to receive(:build)
        subject
      end
    end

    context 'when build human player' do
      let(:type) { :computer }

      it 'build object by interactive player builder' do
        expect(Player::ComputerPlayerBuilder).to receive(:build)
        subject
      end
    end
  end
end
