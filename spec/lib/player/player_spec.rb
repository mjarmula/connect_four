require 'spec_helper'

RSpec.describe Player do
  let(:token) { double(Board::Token, owner: nil) }
  let(:params) { { name: name, color: color, move_strategy: move_strategy } }
  let(:name) { 'John Doe' }
  let(:color) { :red }
  let(:move_strategy) { double(Player::Move::BasicStrategy, tokens: tokens) }
  let(:board) { double(Board) }
  let(:tokens) { [] }

  subject(:instance) { described_class.new(params) }

  before do
    allow(token).to receive(:owner=)
  end

  describe '#make_move' do
    subject { instance.make_move(token: token, board: board) }

    it 'assigns player as an owner of the token' do
      allow(move_strategy).to receive(:make_move)

      expect(token).to receive(:owner=).with(instance)
      subject
    end

    it 'makes move via strategy' do
      expect(move_strategy).to receive(:make_move)
        .with(token: token, board: board)
      subject
    end

    it 'adds token to collection' do
      allow(move_strategy).to receive(:make_move)

      expect { subject }.to change { instance.tokens.size }.from(0).to(1)
    end
  end
end
