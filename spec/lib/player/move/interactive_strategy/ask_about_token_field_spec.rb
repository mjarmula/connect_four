require 'spec_helper'

RSpec.describe Player::Move::InteractiveStrategy::AskAboutTokenField do
  let(:board) { double(Board) }

  describe '.call' do
    subject { described_class.call(available_fields: available_fields) }

    let(:user_input) { '1' }
    let(:field) { double(Board::Field, x: 0, y: 0) }
    let(:available_fields) { [field] }

    before do
      allow_any_instance_of(Validators::InclusionValidator)
        .to receive(:validate!)
      expect_any_instance_of(Prompter).to receive(:ask_about)
        .and_return(user_input)
    end

    it 'validates user input' do
      expect_any_instance_of(Validators::InclusionValidator)
        .to receive(:validate!)
      subject
    end

    it 'returns field for token' do
      expect(subject).to eq(field)
    end
  end
end
