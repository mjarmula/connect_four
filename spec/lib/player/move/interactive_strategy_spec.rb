require 'spec_helper'

RSpec.describe Player::Move::InteractiveStrategy do
  include_examples 'a move strategy'

  let(:available_fields) { [field] }

  before do
    allow(board).to receive(:insert_token)
    allow(Player::Move::InteractiveStrategy::AskAboutTokenField)
      .to receive(:call)
      .and_return(field)
    allow(Detectors::AvailableFieldsDetector).to receive(:find)
      .and_return(available_fields)
    allow(field).to receive(:token=)
    allow(token).to receive(:field=)
  end

  describe '.make_move' do
    subject { described_class.make_move(params) }

    it 'asks user about token position' do
      expect(Player::Move::InteractiveStrategy::AskAboutTokenField)
        .to receive(:call).with(available_fields: available_fields)
        .and_return(field)
      subject
    end

    it 'connects token and field' do
      expect(token).to receive(:field=).with(field)
      expect(field).to receive(:token=).with(token)
      subject
    end
  end
end
