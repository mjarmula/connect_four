require 'spec_helper'

RSpec.describe Player::Move::DummyStrategy do
  include_examples 'a move strategy'

  describe '.make_move' do
    subject { described_class.make_move(params) }

    before do
      allow_any_instance_of(described_class).to receive(:token_field)
        .and_return(field)
    end

    it 'connects token and field' do
      expect(token).to receive(:field=).with(field)
      expect(field).to receive(:token=).with(token)
      subject
    end
  end
end
