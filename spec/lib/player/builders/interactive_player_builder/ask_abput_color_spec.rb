require 'spec_helper'

RSpec.describe Player::InteractivePlayerBuilder::AskAboutColor do
  let(:prompter) { double(Prompter) }
  let(:available_colors) { [:red, :blue] }
  let(:params) { { prompter: prompter, available_colors: available_colors } }
  let(:input_error) { Validators::Errors::InvalidInputError }

  describe '.call' do
    subject { described_class.call(params) }

    before do
      expect(prompter).to receive(:ask_about).and_return(red_color_index)
    end

    let(:red_color_index) { '1' }

    context 'when the user input is numeric and in the color range' do
      it 'expects no errors' do
        expect { subject }.not_to raise_error
      end
    end

    context 'when the user input is numeric but not in the color range' do
      let(:red_color_index) { '0' }

      it 'expects no errors' do
        expect { subject }.to raise_error(input_error)
      end
    end

    context 'when the user input is not numeric' do
      let(:red_color_index) { 'abc' }

      it 'expects no errors' do
        expect { subject }.to raise_error(input_error)
      end
    end

    it 'deletes selector color from the color list' do
      expect { subject }
        .to change { available_colors.size }.by(-1)
    end

    it 'retuns color value' do
      expect(subject).to eq(:red)
    end
  end
end
