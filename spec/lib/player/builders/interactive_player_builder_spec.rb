require 'spec_helper'

RSpec.describe Player::InteractivePlayerBuilder do
  let(:available_colors) { [:red, :green, :blue] }

  describe '.build' do
    subject { described_class.build(available_colors) }

    before do
      expect_any_instance_of(Prompter).to receive(:ask_about)
        .with('Your name ?')
      expect(Player::InteractivePlayerBuilder::AskAboutColor).to receive(:call)
    end

    it 'returns a player object' do
      expect(subject).to be_a Player
    end
  end
end
