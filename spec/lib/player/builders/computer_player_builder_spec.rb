require 'spec_helper'

RSpec.describe Player::ComputerPlayerBuilder do
  let(:available_colors) { [:red] }

  describe '.build' do
    subject { described_class.build(available_colors) }

    it 'returns a player object' do
      expect(subject).to be_a Player
    end

    it 'has random computer name' do
      expect(subject.name).to include('Computer')
    end

    it 'has color from array' do
      expect(subject.color).to eq(:red)
    end

    it 'has dummy movement strategy' do
      expect(subject.send(:move_strategy)).to eq(Player::Move::DummyStrategy)
    end
  end
end
