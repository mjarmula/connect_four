RSpec.shared_examples 'a detector' do
  let(:field1) { double(Board::Field, token?: false, token: nil, x: 0, y: 0) }
  let(:field2) { double(Board::Field, token?: false, token: nil, x: 1, y: 0) }
  let(:field3) { double(Board::Field, token?: false, token: nil, x: 2, y: 0) }
  let(:field4) { double(Board::Field, token?: false, token: nil, x: 3, y: 0) }
  let(:field5) { double(Board::Field, token?: false, token: nil, x: 0, y: 1) }
  let(:field6) { double(Board::Field, token?: false, token: nil, x: 1, y: 1) }
  let(:field7) { double(Board::Field, token?: false, token: nil, x: 2, y: 1) }
  let(:field8) { double(Board::Field, token?: false, token: nil, x: 3, y: 1) }
  let(:field9) { double(Board::Field, token?: false, token: nil, x: 0, y: 2) }
  let(:field10) { double(Board::Field, token?: false, token: nil, x: 1, y: 2) }
  let(:field11) { double(Board::Field, token?: false, token: nil, x: 2, y: 2) }
  let(:field12) { double(Board::Field, token?: false, token: nil, x: 3, y: 2) }
  let(:field13) { double(Board::Field, token?: false, token: nil, x: 0, y: 3) }
  let(:field14) { double(Board::Field, token?: false, token: nil, x: 1, y: 3) }
  let(:field15) { double(Board::Field, token?: false, token: nil, x: 2, y: 3) }
  let(:field16) { double(Board::Field, token?: false, token: nil, x: 3, y: 3) }

  let(:board) { Board.new(cols: 4, rows: 4) }
  let(:matrix) do
    [
      [field1, field2, field3, field4],
      [field5, field6, field7, field8],
      [field9, field10, field11, field12],
      [field13, field14, field15, field16]
    ]
  end

  before do
    allow(board).to receive(:fetch_field).and_call_original
    allow_any_instance_of(Board::Grid).to receive(:matrix).and_return(matrix)
  end
end
