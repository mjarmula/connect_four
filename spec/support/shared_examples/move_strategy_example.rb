RSpec.shared_examples 'a move strategy' do
  let(:board) { double(Board) }
  let(:token) { double(Board::Token) }
  let(:field) { double(Board::Field, x: 0, y: 1) }
  let(:params) { { board: board, token: token } }
end
