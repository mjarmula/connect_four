class Config
  include Singleton

  attr_reader :players_number

  def available_colors
    [:red, :blue, :yellow, :green, :cyan]
  end

  def players
    [:human, :computer]
  end
end
