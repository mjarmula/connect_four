#Description
Ruby console implementation of Connect Four game
https://en.wikipedia.org/wiki/Connect_Four
# Launching
## Install dependencies
`bundle install`
## Run code
`ruby main.js`

# Requirements
Ruby >= `2.3.0`
