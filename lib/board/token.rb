class Board
  class Token
    Symbol = '  '.freeze

    attr_accessor :owner, :field

    def to_s
      Symbol.colorize(background: owner.color)
    end
  end
end
