class Board
  class Grid
    attr_reader :cols, :rows
    private :cols, :rows

    def initialize(cols:, rows:)
      @cols, @rows = cols, rows
    end

    def matrix
      @matrix ||= Array.new(rows) do |row_index|
        Array.new(cols) { |col_index| Field.new(x: col_index, y: row_index) }
      end
    end

    def fetch_field(x:, y:)
      matrix[y][x] if in_matrix_boundaries?(x, y)
    end

    def in_matrix_boundaries?(x, y)
      x.between?(0, cols - 1) && y.between?(0, rows - 1)
    end
  end
end
