class Board
  class Render
    attr_reader :projection_matrix
    private :projection_matrix

    def initialize(original_matrix)
      @projection_matrix = original_matrix.dup
      decorate_board_with_numbers
    end

    def self.call(original_matrix)
      new(original_matrix).render
    end

    def render
      table.to_s
    end

    private

    def table
      @table ||= Terminal::Table.new(rows: projection_matrix) do |table|
        table.style = { all_separators: true }
      end
    end

    def decorate_board_with_numbers
      decorate_vertical
    end

    def decorate_vertical
      projection_matrix.unshift((1..7))
    end
  end
end
