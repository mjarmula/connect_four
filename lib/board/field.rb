class Board
  class Field
    attr_accessor :token
    attr_reader :x, :y

    def initialize(x:, y:, token: nil)
      @x = x
      @y = y
      @token = token
      @value = ' '
    end

    def mark_as_available
      @value = Token::Symbol.on_light_white
    end

    def token?
      !token.nil?
    end

    def to_s
      token ? token.to_s : @value
    end
  end
end
