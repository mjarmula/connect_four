class Board
  extend Forwardable
  def_delegators :grid, :matrix, :fetch_field

  attr_reader :cols, :rows

  def initialize(cols:, rows:)
    @cols = cols
    @rows = rows
  end

  def grid
    @grid ||= Grid.new(cols: cols, rows: rows)
  end

  def show_possible_moves
    possible_moves.each(&:mark_as_available)
  end

  def to_s
    Render.call(matrix)
  end

  private

  def possible_moves
    Detectors::AvailableFieldsDetector.find(board: self)
  end
end
