class Game
  attr_reader :init_state
  private :init_state

  def initialize(init_state: GameStates::InitState)
    @init_state = init_state
  end

  def self.start(**args)
    new(**args).start
  end

  def start
    init_state.start
  end
end
