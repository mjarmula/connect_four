class Validators
  class InclusionValidator
    attr_reader :input, :range
    private :input, :range

    def initialize(input:, range:)
      @input = input
      @range = range
    end

    def validate!
      return if valid?
      raise Errors::InvalidInputError, 'Given input is incorrect'
    end

    private

    def valid?
      range.include?(input)
    end
  end
end
