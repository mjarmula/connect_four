class GameStates
  class InitState
    attr_reader :players, :next_state
    private :players, :next_state

    def initialize(next_state: GameplayState)
      @next_state = next_state
      @players = []
    end

    def self.start(**args)
      new(args).start
    end

    def start
      render
      define_players
      go_to_next_state
    end

    private

    def render
      puts '--==] Welcome to the Connect Four game [==--'
      puts 'Please introduce yourself'
    end

    def define_players
      config.players.each(&method(:add_player))
    end

    def add_player(type)
      players << Player::Factory.get(
        type: type,
        available_colors: available_colors
      )
    end

    def go_to_next_state
      next_state.start(players: players)
    end

    def available_colors
      @available_colors ||= config.available_colors.dup
    end

    def config
      Config.instance
    end
  end
end
