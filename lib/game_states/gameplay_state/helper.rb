class GameStates
  class GameplayState
    class View
      module Helper
        def player_name(player)
          player.name
        end

        def player_tokens_info(player)
          Board::Token::Symbol.colorize(background: player.color)
        end
      end
    end
  end
end
