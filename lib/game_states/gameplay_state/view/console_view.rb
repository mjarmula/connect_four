class GameStates
  class GameplayState
    class View
      class ConsoleView
        include Helper

        def initialize(board:, players:)
          @board, @players = board, players
        end

        def self.render(**args)
          new(args).render
        end

        def render
          system('clear')
          puts message
        end

        private

        def message
          <<~MESSAGE
          #{player_info}
          #{@board}
          MESSAGE
        end

        def player_info
          table = Terminal::Table.new(
            style: { all_separators: true, alignment: :center }
          )
          table.add_row(['Player name:', "Player token's color:"])
          @players.each_with_object(table) do |player, table|
            table.add_row([player_name(player), player_tokens_info(player)])
          end
          table.add_row(
            ['Available moves:', Board::Token::Symbol.on_light_white]
          )
          table
        end
      end
    end
  end
end
