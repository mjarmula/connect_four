class GameStates
  class GameOverState
    attr_reader :winner
    private :winner

    def initialize(winner:)
      @winner = winner
    end

    def self.start(**args)
      new(args).start
    end

    def start
      render
    end

    private

    def render
      puts '---==]Game Over[==---'

      if winner
        puts "#{@winner.name} won, congratulations"
      else
        puts 'There is no winner :('
      end
    end
  end
end
