class GameStates
  class GameplayState
    attr_reader :board, :players, :view, :next_state
    private :board, :players, :view, :next_state

    def initialize(players:, view: View::ConsoleView, next_state: GameOverState)
      @players = players
      @view = view
      @next_state = next_state
      @board = Board.new(cols: 7, rows: 6)
    end

    def self.start(**args)
      new(args).start
    end

    def start
      tours_number.times do
        players.each do |player|
          board.show_possible_moves
          render
          player.make_move(token: Board::Token.new, board: board)
          render
          return go_to_next_state(player) if player.winner?(board)
        end
      end
      go_to_next_state(nil)
    end

    private

    def tours_number
      (board.cols * board.rows / players.size)
    end

    def render
      view.render(board: board, players: players)
    end

    def go_to_next_state(winner)
      next_state.start(winner: winner)
    end
  end
end
