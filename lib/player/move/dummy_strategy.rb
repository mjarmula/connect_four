class Player
  class Move
    class DummyStrategy < BasicStrategy
      private

      def token_field
        @field ||= random_possible_move
      end

      def random_possible_move
        available_fields.sample
      end
    end
  end
end
