class Player
  class Move
    class InteractiveStrategy
      class AskAboutTokenField
        attr_reader :available_fields
        private :available_fields

        def initialize(available_fields:)
          @available_fields = available_fields
        end

        def self.call(**args)
          new(args).call
        end

        def call
          validate_token_position!
          field
        end

        private

        def validate_token_position!
          Validators::InclusionValidator.new(
            input: field,
            range: available_fields
          ).validate!
        end

        def field
          @field ||= available_fields.find do |field|
            field.x == selected_column - 1
          end
        end

        def selected_column
          @selected_column ||= user_output.scan(/([0-9]+)/).flatten.first.to_i
        end

        def user_output
          prompter.ask_about(<<~MESSAGE)
          Please choose column for your token by
          typing column number #{available_columns}
          MESSAGE
        end

        def available_columns
          available_fields.map { |field| field.x + 1 }.sort
        end

        def prompter
          helper.prompter
        end

        def helper
          Helpers.instance
        end
      end
    end
  end
end
