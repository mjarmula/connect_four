class Player
  class Move
    class BasicStrategy
      attr_reader :token, :board
      private :token, :board

      def initialize(board:, token:)
        @board, @token = board, token
      end

      def self.make_move(**params)
        new(params).make_move
      end

      def make_move
        connect_token_with_field
      end

      private

      def connect_token_with_field
        token.field = token_field
        token_field.token = token
      end

      def available_fields
        @available_fields ||= Detectors::AvailableFieldsDetector.find(
          board: board
        )
      end

      def token_field
        raise NotImplementedError, 'class must implelemnt abstract ' \
        'method token_field'
      end
    end
  end
end
