class Player
  class Move
    class InteractiveStrategy < BasicStrategy
      private

      def token_field
        @field ||= AskAboutTokenField.call(available_fields: available_fields)
      rescue Validators::Errors::InvalidInputError => e
        puts e.message
        token_field
      end
    end
  end
end
