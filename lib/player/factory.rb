class Player
  class Factory
    attr_reader :type, :available_colors
    private :type, :available_colors

    def initialize(type:, available_colors:)
      @type, @available_colors = type, available_colors
    end

    def self.get(**args)
      new(args).get
    end

    def get
      player_builder.build(available_colors)
    end

    private

    def player_builder
      defined_builders.fetch(type)
    end

    def defined_builders
      {
        human: Player::InteractivePlayerBuilder,
        computer: Player::ComputerPlayerBuilder
      }
    end

    def config
      Config.instance
    end
  end
end
