class Player
  class InteractivePlayerBuilder
    class AskAboutColor
      attr_reader :prompter, :available_colors
      private :prompter, :available_colors

      def initialize(prompter:, available_colors:)
        @prompter, @available_colors = prompter, available_colors
      end

      def self.call(**args)
        new(args).call
      end

      def call
        validate_color_input!
        available_colors.delete_at(user_output - 1)
      end

      private

      def validate_color_input!
        Validators::InclusionValidator.new(
          input: user_output,
          range: (1..available_colors.size)
        ).validate!
      end

      def user_output
        @user_output ||= prompter.ask_about(color_message).to_i
      end

      def color_message
        "Specify your color (by color number)\n".tap do |message|
          available_colors.each_with_index do |color, index|
            message << "#{index + 1}) #{color.to_s.colorize(color)}\n"
          end
        end
      end
    end
  end
end
