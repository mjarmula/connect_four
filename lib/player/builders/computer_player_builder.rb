class Player
  class ComputerPlayerBuilder < BasicPlayerBuilder
    private

    def name
      "Computer ##{Random.new_seed.to_s[0..4]}"
    end

    def color
      available_colors.pop
    end

    def move_strategy
      Player::Move::DummyStrategy
    end
  end
end
