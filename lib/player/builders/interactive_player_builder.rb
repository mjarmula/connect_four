class Player
  class InteractivePlayerBuilder < BasicPlayerBuilder
    private

    def name
      prompter.ask_about('Your name ?')
    end

    def move_strategy
      Player::Move::InteractiveStrategy
    end

    def color
      AskAboutColor.call(
        prompter: prompter,
        available_colors: available_colors
      )
    rescue Validators::Errors::InvalidInputError => e
      puts e.message
      color
    end

    def prompter
      helpers.prompter
    end

    def helpers
      Helpers.instance
    end
  end
end
