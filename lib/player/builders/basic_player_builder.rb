class Player
  class BasicPlayerBuilder
    attr_reader :available_colors
    private :available_colors

    def initialize(available_colors)
      @available_colors = available_colors
    end

    def self.build(available_colors)
      new(available_colors).build
    end

    def build
      Player.new(
        name: name,
        color: color,
        move_strategy: move_strategy
      )
    end

    private

    def name
      raise NotImplementedError, 'abstract method name must be implemented'
    end

    def color
      raise NotImplementedError, 'abstract method color must be implemented'
    end

    def move_strategy
      raise NotImplementedError,
            'abstract method move_strategy must be implemented'
    end
  end
end
