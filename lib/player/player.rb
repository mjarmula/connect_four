class Player
  attr_reader :name, :color, :tokens, :move_strategy
  private :move_strategy

  def initialize(name:, color:, move_strategy:)
    @name = name
    @color = color
    @move_strategy = move_strategy
    @tokens = []
  end

  def make_move(token:, board:)
    token.owner = self
    tokens << token
    move_strategy.make_move(token: token, board: board)
  end

  def winner?(board)
    Detectors::WinnerDetector.connected_four?(board: board, player: self)
  end
end
