class Helpers
  include Singleton

  def prompter
    @prompter ||= Prompter.new
  end
end
