class Detectors
  class WinnerDetector
    class DiagonalRightDetector < BasicDetector
      private

      def fields
        [
          board.fetch_field(x: field.x + 1, y: field.y - 1),
          board.fetch_field(x: field.x + 2, y: field.y - 2),
          board.fetch_field(x: field.x + 3, y: field.y - 3)
        ]
      end
    end
  end
end
