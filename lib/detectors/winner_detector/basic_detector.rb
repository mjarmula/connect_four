class Detectors
  class WinnerDetector
    class BasicDetector
      attr_reader :board, :field, :player
      private :board, :field, :player

      def initialize(board:, field:, player:)
        @board, @field, @player = board, field, player
      end

      def self.check(**args)
        new(args).check
      end

      def check
        fields.all?(&method(:check_field))
      end

      private

      def check_field(field)
        field_exists?(field) && field_belongs_to_player?(field)
      end

      def field_belongs_to_player?(field)
        field.token && field.token.owner == player
      end

      def field_exists?(field)
        !!field
      end

      def fields
        raise NotImplementedError, 'abstract method fields must be implemented'
      end
    end
  end
end
