class Detectors
  class WinnerDetector
    class HorizontalDetector < BasicDetector
      private

      def fields
        [
          board.fetch_field(x: field.x - 1, y: field.y),
          board.fetch_field(x: field.x - 2, y: field.y),
          board.fetch_field(x: field.x - 3, y: field.y)
        ]
      end
    end
  end
end
