class Detectors
  class WinnerDetector
    class VerticalDetector < BasicDetector
      private

      def fields
        [
          board.fetch_field(x: field.x, y: field.y - 1),
          board.fetch_field(x: field.x, y: field.y - 2),
          board.fetch_field(x: field.x, y: field.y - 3)
        ]
      end
    end
  end
end
