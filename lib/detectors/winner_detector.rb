class Detectors
  class WinnerDetector
    attr_reader :board, :player
    private :board, :player

    def initialize(board:, player:)
      @board, @player = board, player
    end

    def self.connected_four?(**args)
      new(args).connected_four?
    end

    def connected_four?
      player_fields.any? do |field|
        check_horizontal_connection(field)        ||
        check_vertical_connection(field)          ||
        check_diagonal_right_connection(field)    ||
        check_diagonal_left_connection(field)
      end
    end

    private

    def player_fields
      @player_fields ||= player.tokens.map(&:field)
    end

    def check_horizontal_connection(field)
      HorizontalDetector.check(board: board, field: field, player: player)
    end

    def check_vertical_connection(field)
      VerticalDetector.check(board: board, field: field, player: player)
    end

    def check_diagonal_left_connection(field)
      DiagonalLeftDetector.check(board: board, field: field, player: player)
    end

    def check_diagonal_right_connection(field)
      DiagonalRightDetector.check(
        board: board,
        field: field,
        player: player
      )
    end
  end
end
