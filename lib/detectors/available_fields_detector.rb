class Detectors
  class AvailableFieldsDetector
    attr_reader :board
    private :board

    def initialize(board:)
      @board = board
    end

    def self.find(**args)
      new(args).find
    end

    def find
      board.matrix.flatten.select { |field| field_available?(field) }
    end

    private

    def field_available?(field)
      empty_field_in_first_row?(field) || empty_field_above_taken_field?(field)
    end

    def empty_field_in_first_row?(field)
      (!field.token? && field.y == board.rows - 1)
    end

    def empty_field_above_taken_field?(field)
      (!field.token? && board.matrix[field.y + 1][field.x].token?)
    end
  end
end
